const express = require('express');
const mongoose = require('mongoose');
const path = require('path');
const bodyParser = require('body-parser');

const config = require('./config/config');

const app = express();

// Middlewares
app.use(bodyParser.urlencoded({ extended: false })); // parse application/x-www-form-urlencoded
app.use(bodyParser.json()); // parse application/json

// Enable public
app.use(express.static(path.resolve(__dirname, '../public')));

// Routes
app.use(require('./routes/index'));


mongoose.connect(config.urlDB, {
    useCreateIndex: true,
    useNewUrlParser: true
}, (err, res) => {
    if (err) throw err;

    console.log('Base de datos corriendo');
});

app.listen(config.port, () => {
    console.log(`Escuchando puerto ${config.port}`);
});