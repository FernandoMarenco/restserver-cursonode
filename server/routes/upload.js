const express = require('express');
const fileUpload = require('express-fileupload');
const fs = require('fs');
const path = require('path');

const User = require('../models/user');
const Product = require('../models/product');

const { verifyToken } = require('../middlewares/authentication');

const app = express();

//default options
app.use(fileUpload({ useTempFiles: true }));

app.put('/upload/:type/:id', verifyToken, async (req, res, next) => {

    const { type, id } = req.params;

    //validar type
    let types = ['products', 'users'];
    if(types.indexOf(type) < 0) {
        return res.status(400).json({
            ok: false,
            err: {
                message: 'Tipo no válido, solo se acepta: ' + types.join(', ')
            }
        });
    }

    if(!req.files) {
        return res.status(400).json({
            ok: false,
            err: {
                message: 'No se selecciono ningún archivo'
            }
        });
    }

    let avatar = req.files.avatar;

    let fileSections = avatar.name.split('.');
    let fileExt = fileSections[fileSections.length-1];
    console.log(fileSections, fileExt);

    //extenciones permitidas
    let validExts = ['png', 'jpg', 'gif'];
    if(validExts.indexOf(fileExt) < 0) {
        return res.status(400).json({
            ok: false,
            err: {
                message: 'La extensión del archivo no es válido, solo se acepta: ' + validExts.join(', ')
            }
        });
    }

    //configurar nombre del archivo
    const fileName = `${id}-${new Date().getTime()}.${fileExt}`;

    //cargar imagen
    let updated;
    if(type === 'users') {
        updated = await updateImageUser(id, fileName).catch((err) => {
            return res.status(400).json({
                ok: false,
                err
            });
        });
    }
    else if(type === 'products') {
        updated = await updateImageProduct(id, fileName).catch((err) => {
            return res.status(400).json({
                ok: false,
                err
            });
        });
    }

    if(updated) {
        if(updated.img) {
            deleteImage(type, updated.img);
        }

        avatar.mv(`uploads/${type}/${fileName}`, (err) => {
            if(err) {
                return res.status(500).json({
                    ok: false,
                    err
                });
            }
        });

        res.json({
            ok: true,
            message: 'Imagen subida correctamente',
            img: fileName
        });
    }

    next();

});

const updateImageUser = async (id, fileName) => {
    return await User.findOneAndUpdate({ _id: id }, { img: fileName });
}

const updateImageProduct = async (id, fileName) => {
    return await Product.findOneAndUpdate({ _id: id }, { img: fileName });
}

const deleteImage = (type, fileName) => {
    const imagePath = path.resolve(__dirname, `../../uploads`, type, fileName);
    console.log(imagePath);
    if(fs.existsSync(imagePath)) {
        fs.unlinkSync(imagePath);
    }
}

module.exports = app;