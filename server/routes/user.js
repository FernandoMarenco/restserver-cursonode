const bcrypt = require('bcrypt');
const _ = require('underscore');

const express = require('express');
const app = express();

const User = require('../models/user');
const { verifyToken, verifyAdmin } = require('../middlewares/authentication');


app.get('/users', verifyToken, (req, res) => {

    let page = Number(req.query.page) || 1;
    let limit = Number(req.query.limit) || 5;

    User
    // .find({})
    // .skip(from)
    // .limit(limit)
        .paginate({ state: true }, { page, limit, select: '-password' }, (err, data) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                err
            });
        }

        res.json({
            ok: true,
            total: data.total,
            users: data.docs
        });
    });

});

app.post('/user', [verifyToken, verifyAdmin], (req, res) => {
    let body = req.body;

    let user = new User({
        name: body.name,
        email: body.email,
        password: bcrypt.hashSync(body.password, 10),
        role: body.role
    });

    user.save((err, u) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                err
            });
        }

        // u.password = null;

        res.json({
            ok: true,
            user: u
        })

    });

});

app.put('/user/:id', [verifyToken, verifyAdmin], (req, res) => {
    let id = req.params.id;
    let body = _.pick(req.body, [
        'name',
        'email',
        'img',
        'role',
        'state'
    ]);

    // forma ineficiente
    // delete body.google;
    // delete body.password;

    User.findOneAndUpdate(id, body, { context: 'query', new: true, runValidators: true }, (err, u) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                err
            });
        }

        res.json({
            ok: true,
            user: u
        });
    });


});

app.delete('/user/:id', [verifyToken, verifyAdmin], (req, res) => {
    let id = req.params.id;

    // eliminar registro completamente
    // User.findByIdAndRemove(id, (err, u) => {

    // eliminar editando el estado
    let del = {
        state: false
    };

    User.findOneAndUpdate(id, del, { new: true }, (err, u) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                err
            });
        }

        if (!u) {
            return res.status(404).json({
                ok: false,
                err: {
                    message: 'Usuario no encontrado'
                }
            });
        }

        res.json({
            ok: true,
            user: u
        })
    });
});

module.exports = app;