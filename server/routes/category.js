const express = require('express');

const { verifyToken, verifyAdmin } = require('../middlewares/authentication');
let Category = require('../models/category');
let app = express();

app.get('/category', verifyToken, (req, res) => {
    Category.find({})
    .sort('description')
    .populate('user', 'name email')
    .exec((err, categories) => {
        if(err) {
            return res.status(500).json({
                ok: false,
                err
            });
        }

        res.json({
            ok: true,
            categories
        });
    });
});

app.get('/category/:id', verifyToken, (req, res) => {
    const categoryId = req.params.id;

    Category.findById(categoryId, (err, category) => {
        if(err) {
            return res.status(500).json({
                ok: false,
                err
            });
        }

        if(!category) {
            return res.status(404).json({
                ok: false,
                err: {
                    message: 'Categoria no encontrada'
                }
            });
        }

        res.json({
            ok: true,
            category
        });
    });
});

app.post('/category', verifyToken, (req, res) => {
    const { description } = req.body;

    let category = new Category({
        description,
        user: req.user._id
    });

    category.save((err, c) => {
        if(err) {
            return res.status(500).json({
                ok: false,
                err
            });
        }

        if(!c) {
            return res.status(400).json({
                ok: false,
                err
            });
        }

        res.json({
            ok: true,
            category: c
        });
    });
});

app.put('/category/:id', verifyToken, (req, res) => {
    const categoryId = req.params.id;
    const { description } = req.body;

    Category.findOneAndUpdate({ _id: categoryId }, { description }, { new: true, runValidators: true }, (err, c) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                err
            });
        }

        if(!c) {
            return res.status(400).json({
                ok: false,
                err
            });
        }

        res.json({
            ok: true,
            category: c
        });
    });
});

app.delete('/category/:id', [verifyToken, verifyAdmin], (req, res) => {
    const categoryId = req.params.id;

    Category.findOneAndRemove({ _id: categoryId}, (err, c) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                err
            });
        }

        if(!c) {
            return res.status(400).json({
                ok: false,
                err: {
                    message: 'El ID no existe'
                }
            });
        }

        res.json({
            ok: true,
            message: 'Categoria eliminada'
        })
    });
});

module.exports = app;