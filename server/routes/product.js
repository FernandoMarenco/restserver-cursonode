const express = require('express');
const { verifyToken } = require('../middlewares/authentication');

let app = express();
let Product = require('../models/product');

//obtener todos los productos
//popular usuario y categoria
//paginado
app.get('/product', verifyToken, (req, res) => {
    const page = Number(req.query.page) || 1;
    const limit = Number(req.query.limit) || 5;

    const populate = [
        {path: 'user', select: 'name email'}, 
        {path: 'category', select: 'description'}
    ];

    Product.paginate({ state: true }, {
        page,
        limit,
        populate,
        sort: 'name'
    }, (err, data) => {
        if(err) {
            return res.status(500).json({
                ok: false,
                err
            });
        }

        res.json({
            ok: true,
            data
        });
    });
});


//obtener un producto por id
//popular todo
app.get('/product/:id', verifyToken, (req, res) => {
    const productId = req.params.id;

    Product.find({ _id: productId })
    .populate('user', 'name email')
    .populate('category', 'description')
    .exec((err, p) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                err
            });
        }

        if (!p) {
            return res.status(404).json({
                ok: false,
                err: {
                    message: 'Producto no encontrado'
                }
            });
        }

        res.json({
            ok: true,
            product: p
        })
    });
});

//buscar producto
app.get('/product/search/:text', verifyToken, (req, res)=> {
    const text = req.params.text;

    //regex
    let regex = new RegExp(text, 'i');

    Product.find({ name: regex })
    .populate('category', 'description')
    .exec((err, products) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                err
            });
        }

        res.json({
            ok: true,
            products
        })
    });
});

//crear producto
//usuario y categoria
app.post('/product', verifyToken, (req, res) => {
    const { name, category, priceUni, description } = req.body;

    let product = new Product({
        user: req.user._id,
        name,
        category,
        priceUni,
        description
    });

    product.save((err, product) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                err
            });
        }

        res.status(201).json({
            ok: true,
            product
        })
    });
});

//actualizar producto
//usuario y categoria
app.put('/product/:id', verifyToken, (req, res) => {
    const productId = req.params.id;
    const { name, category, priceUni, description } = req.body;
    const body = req.body;

    Product.findOneAndUpdate({ _id: productId }, body, { new: true, runValidators: true }, (err, p) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                err
            });
        }

        if (!p) {
            return res.status(404).json({
                ok: false,
                err: {
                    message: 'Producto no encontrado'
                }
            });
        }

        res.json({
            ok: true,
            product: p
        });
    });

});

//borrar producto
//poner false state
app.delete('/product/:id', verifyToken, (req, res) => {
    let id = req.params.id;

    Product.findOneAndUpdate({ _id: id }, {
        state: false
    }, { new: true }, (err, p) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                err
            });
        }

        if (!p) {
            return res.status(404).json({
                ok: false,
                err: {
                    message: 'Producto no encontrado'
                }
            });
        }

        res.json({
            ok: true,
            product: p,
            message: 'Producto borrado'
        });
    });
});


module.exports = app;