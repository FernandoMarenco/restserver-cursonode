const express = require('express');
const app = express();

// login
app.use(require('./login'));

// user
app.use(require('./user'));

// category
app.use(require('./category'));

// product
app.use(require('./product'));

// upload files
app.use(require('./upload'));

// show images
app.use(require('./images'));

module.exports = app;