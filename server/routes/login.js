const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const express = require('express');
const { OAuth2Client } = require('google-auth-library');

let Usuario = require('../models/user');
const config = require('../config/config');

const app = express();
const client = new OAuth2Client(config.googleClientId);

app.post('/login', (req, res) => {
    let body = req.body;

    Usuario.findOne({ email: body.email }, (err, u) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                err
            });
        }

        if (!u) {
            return res.status(400).json({
                ok: false,
                err: {
                    message: '(Usuario) o contraseña incorrectos'
                }
            });
        }

        if (!bcrypt.compareSync(body.password, u.password)) {
            return res.status(400).json({
                ok: false,
                err: {
                    message: 'Usuario o (contraseña) incorrectos'
                }
            });
        }

        let token = jwt.sign({
            user: u
        }, config.seedToken, { expiresIn: config.expToken });

        res.json({
            ok: true,
            user: u,
            token
        });
    })

});

// Configuraciones de Google
async function verify(token) {
    const ticket = await client.verifyIdToken({
        idToken: token,
        audience: config.googleClientId,  // Specify the CLIENT_ID of the app that accesses the backend
    });
    const payload = ticket.getPayload();
    
    return {
        name: payload.name,
        email: payload.email,
        img: payload.picture,
        google: true
    }
}

app.post('/google', async (req, res) => {
    const { googleToken } = req.body;

    let googleUser = await verify(googleToken)
    .catch((err) => {
        res.status(403).json({
            ok: false,
            err
        })
    });

    Usuario.findOne({ email: googleUser.email }, (err, u) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                err
            });
        }

        if (u) { // si existe el usuario
            if (u.google === false) { // se autentico normal
                return res.status(400).json({
                    ok: false,
                    err: { message: 'Debe utilizar su autenticación normal'}
                });
            }
            else { // se autentico con google
                let token = jwt.sign({
                    user: u
                }, config.seedToken, { expiresIn: config.expToken });

                return res.json({
                    ok: true,
                    usuario: u,
                    token
                });
            }
        }
        else { // si el usuario no existe en la base de datos
            
            let usuario = new Usuario();

            usuario.name = googleUser.name;
            usuario.email = googleUser.email;
            usuario.img = googleUser.img;
            usuario.google = true;
            usuario.password = ':)';

            usuario.save((err, u) => {
                if (err) {
                    return res.status(500).json({
                        ok: false,
                        err
                    });
                }

                let token = jwt.sign({
                    user: u
                }, config.seedToken, { expiresIn: config.expToken });

                return res.json({
                    ok: true,
                    usuario: u,
                    token
                });
            });
        }

    });
});


module.exports = app;