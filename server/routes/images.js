const express = require('express');
const fs = require('fs');
const path = require('path');

const { verifyTokenImg } = require('../middlewares/authentication');

const app = express();

app.get('/image/:type/:img', verifyTokenImg, (req, res) => {
    const { type, img } = req.params;

    const imagePath = path.resolve(__dirname, `../../uploads/${type}/${img}`);

    if(fs.existsSync(imagePath)) {
        res.sendFile(imagePath);
    } else {
        const noImagePath = path.resolve(__dirname, '../assets/no-image.jpg');
        res.sendFile(noImagePath);
    }
});


module.exports = app;