const keys = (!process.env.PORT) ? require('../../secrets/keys') : null;

// port
const port = process.env.PORT || 3000;

// mongoDB 
const nodeEnv = process.env.NODE_ENV || 'dev';

let urlDB;
if (nodeEnv === 'dev') {
    urlDB = 'mongodb://localhost:27017/cafe'
} else {
    urlDB = process.env.MONGO_URI;
}

// token expire 60 seg * 60 min * 24 hrs * 1 day
const expToken = '24h'; //60 * 60 * 24 * 1;
//process.env.EXP_TOKEN = expToken;

// seed authentication
const seedToken = process.env.SEED_TOKEN || keys.SEED_TOKEN;

// Google Client ID
const googleClientId = process.env.GOOGLE_CLIENT_ID || keys.GOOGLE_CLIENT_ID;

module.exports = {
    port,
    urlDB,
    expToken,
    seedToken,
    googleClientId
}