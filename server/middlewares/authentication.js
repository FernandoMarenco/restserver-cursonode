const jwt = require('jsonwebtoken');

const config = require('../config/config');

// next continuar con la ejecucion
let verifyToken = (req, res, next) => {

    let token = req.get('Authorization');

    jwt.verify(token, config.seedToken, (err, decoded) => {
        if (err) {
            return res.status(401).json({
                ok: false,
                err: {
                    message: 'Invalid token'
                }
            })
        }

        req.user = decoded.user;
        next();
    });

};

let verifyAdmin = (req, res, next) => {
    let user = req.user;

    if (user.role !== 'ADMIN_ROLE') {
        return res.status(401).json({
            ok: false,
            err: {
                message: 'No tienes permiso de realizar esta acción'
            }
        })
    }

    next();
};

const verifyTokenImg = (req, res, next) => {
    const { token } = req.query;

    jwt.verify(token, config.seedToken, (err, decoded) => {
        if (err) {
            return res.status(401).json({
                ok: false,
                err: {
                    message: 'Invalid token'
                }
            })
        }

        req.user = decoded.user;
        next();
    });
}

module.exports = {
    verifyToken,
    verifyAdmin,
    verifyTokenImg
};