const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const mongoosePaginate = require('mongoose-paginate');

let productSchema = new Schema({
    name: { type: String, required: [true, 'El nombre es necesario'] },
    priceUni: { type: Number, required: [true, 'El precio unitario es necesario'] },
    description: { type: String, required: false },
    img: { type: String, required: false },
    state: { type: Boolean, required: true, default: true },
    category: { type: Schema.Types.ObjectId, ref: 'Category' },
    user: { type: Schema.Types.ObjectId, ref: 'User' }
});

productSchema.plugin(mongoosePaginate);

module.exports = mongoose.model('Product', productSchema);