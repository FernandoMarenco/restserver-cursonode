const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let categorySchema = new Schema({
    user: { type: Schema.Types.ObjectId, ref: 'User' },
    description: { type: String, required: [true, 'La descripción debe ser obligatoria'] }
});

module.exports = mongoose.model('Category', categorySchema);